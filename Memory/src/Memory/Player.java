package Memory;

import javax.swing.JLabel;

public class Player extends JLabel {

	private static final long serialVersionUID = 1L;
	private int points = 0;

	public Player() {
		super();
	}

	public void addPoint() {
		this.points += 1;
	}

	public int getPoints() {
		return this.points;
	}

}
