package Memory;

import javax.swing.JButton;

public class Cards extends JButton {

	private static final long serialVersionUID = 1L;
	private String underName;
	private boolean geloest = false;

	public Cards(String text) {
		this.setText(text);
	}

	public void setUnderName(String x) {
		this.underName = x;
	}

	public String getUnderName() {
		return this.underName;
	}

	public void setGeloest(boolean geloest) {
		this.geloest = geloest;
	}

	public boolean getGeloest() {
		return this.geloest;
	}
}
