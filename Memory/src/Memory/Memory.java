package Memory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Memory extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel pnlCenter = new JPanel();
	private JPanel pnlSouth = new JPanel();

	private int aufdecken = 2;
	private String firstAufgedeckt, secondAufgedeckt;

	private Cards card;
	private String MEMORY_STRING = "MEMORY";
	private ArrayList<Cards> memoryList = new ArrayList<Cards>();
	private ArrayList<String> memoryListNames = new ArrayList<String>();

	private ArrayList<Player> playerList = new ArrayList<Player>();
	// Player 1 is true
	// Player 2 is false
	private boolean whoPlays = false;

	public Memory() {
		super();

		this.setTitle("Memory");
		this.setSize(500, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		pnlCenter.setLayout(new GridLayout(8, 8));
		pnlSouth.setLayout(new FlowLayout());

		createButtonNames();
		createButtons();
		addButtonsToLayout(pnlCenter);

		createPlayers();
		createPlayerNames();
		addPlayerToLayout(pnlSouth);

		cp.add(pnlCenter, BorderLayout.CENTER);
		cp.add(pnlSouth, BorderLayout.SOUTH);

		this.setVisible(true);
	}

	private void createButtonNames() {
		String[] names = { "Katze", "Hund", "Fisch", "Raubkatze", "Vogel",
				"Papagei", "Wurst", "Karotte", "Wurm", "Kirsch", "String",
				"Integer", "Liste", "42", "12", "Hans", "Stefan", "Corina",
				"Agentur", "Android", "iOS", "Windows Phone", "Java", "Python",
				"JButton", "Grill", "Teller", "Messer", "Gabel", "Würfel",
				"KitKat", "Balisto" };

		for (int i = 0; i < names.length; ++i) {
			memoryListNames.add(names[i]);
			memoryListNames.add(names[i]);
		}

		Collections.shuffle(memoryListNames);
	}

	private void addButtonsToLayout(JPanel pnl) {
		for (int i = 0; i < memoryList.size(); ++i) {
			pnl.add(memoryList.get(i));
		}
	}

	private void createButtons() {
		for (int i = 0; i < 64; ++i) {
			Cards card = new Cards(MEMORY_STRING);
			card.setUnderName(memoryListNames.get(i));
			card.addActionListener(this);
			memoryList.add(card);
		}
	}

	private void createPlayers() {
		playerList.add(new Player());
		playerList.add(new Player());
	}

	private void createPlayerNames() {
		playerList.get(0).setText(
				"Player 1 - Punkte: " + playerList.get(0).getPoints());
		playerList.get(0).setForeground(Color.RED);
		playerList.get(1).setText(
				"Player 2 - Punkte: " + playerList.get(1).getPoints());
		playerList.get(1).setForeground(Color.BLUE);
	}

	private void addPlayerToLayout(JPanel pnl) {
		pnl.add(playerList.get(0));
		pnl.add(playerList.get(1));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Cards card = (Cards) e.getSource();
		boolean yes = true;
		if (card.equals(this.card)) {
			yes = false;
		}
		this.card = card;
		if (yes) {

			if (aufdecken == 0) {
				for (int i = 0; i < memoryList.size(); ++i) {
					Cards cards = memoryList.get(i);
					boolean geloest = cards.getGeloest();

					if (geloest) {
						memoryList.get(i).setText(
								memoryList.get(i).getUnderName());
					} else {
						memoryList.get(i).setText(MEMORY_STRING);
					}
				}
				aufdecken = 2;
			}

			card.setText(card.getUnderName());

			if (aufdecken == 2) {
				firstAufgedeckt = card.getText();
			} else if (aufdecken == 1) {
				secondAufgedeckt = card.getText();
			}

			boolean findACard = false;
			if (firstAufgedeckt.equals(secondAufgedeckt)) {
				findACard = true;
				if (!whoPlays) {
					playerList.get(0).addPoint();
					playerList.get(0).setText(
							"Player 1 - Punkte: "
									+ playerList.get(0).getPoints());
				} else {
					playerList.get(1).addPoint();
					playerList.get(1).setText(
							"Player 2 - Punkte: "
									+ playerList.get(1).getPoints());
				}
				for (int i = 0; i < memoryList.size(); ++i) {
					Cards c = memoryList.get(i);
					if (c.getText().equals(firstAufgedeckt)) {
						c.setGeloest(true);
						c.setText(c.getUnderName());
						if (!whoPlays) {
							c.setBackground(Color.MAGENTA);
						} else {
							c.setBackground(Color.CYAN);
						}
					}
				}
			}

			aufdecken -= 1;

			if (aufdecken == 0) {
				firstAufgedeckt = "null";
				secondAufgedeckt = "null";

				if (!(findACard)) {
					if (whoPlays) {
						playerList.get(0).setForeground(Color.RED);
						playerList.get(1).setForeground(Color.BLUE);
					} else {
						playerList.get(0).setForeground(Color.BLUE);
						playerList.get(1).setForeground(Color.RED);
					}

					whoPlays = !whoPlays;
				}
			}
		}
	}

	public static void main(String[] args) {
		new Memory();
	}

}
